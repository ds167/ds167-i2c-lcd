#!/bin/bash

#Below 3rd and 4th lines are for larger displays

LINE1="Line 1"
LINE2="Line 2"
LINE3="Line 3"
LINE4="Line 4"
./i2c_lcd

COUNTER=1000
while [ $((COUNTER--)) -gt 0 ]
do
	echo "$COUNTER"
	LINE1=$(date +"%H:%M:%S")
	LINE2=$(date +"%d:%m:%Y")

   	./i2c_lcd "${LINE1}" "${LINE2}" "${LINE3}" "${LINE4}"
done
exit

for (( i=1; i<=10000; i++ )); do
    LINE2=`date +"%d.%m.%Y-%H:%M:%S"`
   ./i2c_lcd "Counter ${i}" "${LINE2}" "${LINE3}" "${LINE4}"
done
